import React, {Component} from 'react';
import './myProfile.less';

class MyProfile extends Component {

  constructor(props) {
    super(props);
    this.state = {
      name: '',
      gender: '',
      description: '',
      read: false
    }
    this.changeName = this.changeName.bind(this);
    this.changeDescription = this.changeDescription.bind(this);
    this.changeGender = this.changeGender.bind(this);
    this.changeRead = this.changeRead.bind(this);
    this.submitFunc = this.submitFunc.bind(this);
  }

  changeName(event){
    this.setState({name: event.target.value});
    console.log(this.state.name);
  }

  changeDescription(event){
    this.setState({description: event.target.value});
    console.log(this.state.description);
  }

  changeGender(event){
    this.setState({gender: event.target.value});
    console.log(this.state.gender);
  }

  changeRead(event){
    this.setState({read: event.target.value});
    console.log(this.state.read);
  }

  submitFunc(event){
    event.preventDefault();
    const {name, gender, description, read} = this.state;
    console.log(name, gender, description, read);
  }



  render() {
    const {name, gender, description, read} = this.state;
    return (
      <form onSubmit={this.submitFunc}>
        <label>Name</label>
        <input type={'text'} value={name} onChange={this.changeName}/>
        <label>Gender</label>
        <select onChange={this.changeGender}>
          <option value={'Male'}>Male</option>
          <option value={'Female'}>Female</option>
        </select>
        <label>Description</label>
        <textarea value={description} onChange={this.changeDescription}></textarea>
        <input type={'checkbox'} value={read} onChange={this.changeRead}/>I have read
        <input type={'submit'}/>
      </form>
    );
  }
}

export default MyProfile;


